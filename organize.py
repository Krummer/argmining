#!/usr/bin/python3

import sys
import os
import json
from xml.etree.ElementTree import parse


# parse a xml file, returns a list of samples (one or more)
def data_from_xml(xml_tree):
    # each sample has a claim, an one or more statements. Each statement needs a label of 0: support, 1: attack.
    # to preserve order of statements, just mark the index that holds the claim?
    # could be bad for other corpora?

    xml_root = xml_tree.getroot()

    # micro-text corpus file
    if xml_root.tag == "arggraph":
        data = {"claim": 0, "statements": [], "labels": []}
        claim_candidates = []
        # go through all tags
        for child in xml_root:
            # add statements and labels in same order
            if child.tag == "edu":
                text = child.text.replace("<![CDATA[", "").replace("]]>", "")
                data["statements"].append(text)
            if child.tag == "adu":
                lbl = child.attrib["type"]
                data["labels"].append(0 if lbl == "pro" else 1)
                claim_candidates.append(child.attrib["id"])
            if child.tag == "edge":
                # remove all candidates that are used as source. The claim has no outgoing connection
                src = child.attrib["src"]
                if src in claim_candidates:
                    claim_candidates.remove(src)
        # there must be a last remaining candidate, otherwise something is wrong.
        data["claim"] = int(claim_candidates[0][1:]) - 1
        return [data]

    # TODO: debatepedia file. may need sentence tokenizer ? Or just take single sentence stuff.
    elif xml_root.tag == "entailment-corpus":
        result = []
        for child in xml_root:
            if child.attrib["entailment"] != "UNKNOWN":
                data = {"claim": 0, "statements": [], "labels": []}
                noentail = "NO" in child.attrib["entailment"] or "CON" in child.attrib["entailment"]
                data["statements"].append(child.find('h').text)
                data["statements"].append(child.find('t').text)
                data["labels"].append(1 if noentail else 0)
                result.append(data)
        return result


# parse all xml files in folder
def gather_from_folder(path):
    data_list = []
    for file_path in [os.path.join(path, fn) for fn in next(os.walk(path))[2]]:
        if file_path.endswith(".xml"):
            print("parsing: " + file_path)
            data_list += data_from_xml(parse(file_path))

    return data_list


# stores all samples as json file
def save_as_json(sample_list, file_name):
    with open(file_name, 'w', encoding='utf-8') as j_file:
        json.dump(sample_list, j_file, indent=4, ensure_ascii=False)


# convert data to json format, for easier handling.
if __name__ == "__main__":
    if "-h" in sys.argv:
        print("<path-to-folder(s) or xml file> [required]")
        print("-o <file-name-output>")

    in_path = str(sys.argv[1])
    out_name = sys.argv[sys.argv.index("-o") + 1] if "-o" in sys.argv else "out.json"

    if in_path.endswith(".xml") and os.path.exists(in_path):
        save_as_json(data_from_xml(parse(in_path)), out_name)
    else:
        save_as_json(gather_from_folder(in_path), out_name)

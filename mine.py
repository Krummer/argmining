#!/usr/bin/python3

import sys
import utils.mining as um
from utils.data import load_samples


if __name__ == "__main__":
    if "-h" in sys.argv:
        print("<path-to-data> (json) [required]")

    print("loading data ...")
    # load samples
    data, labels = load_samples(str(sys.argv[1]))
    mine = um.Mine(data, labels)

    print("plotting ...")
    mine.plot_pos()

    '''
    mine.plot_grid((
        (um.Mine.plot_lem, ),
        (um.Mine.plot_pos, )
    ))
    '''

    print("quit.")

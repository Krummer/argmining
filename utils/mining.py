#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from .extraction import FeatureExtractor
from nltk import word_tokenize


class Mine():
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels
        self.extra = FeatureExtractor(self.data, self.labels)

        self.colors = ['r', 'g', 'b', 'y']

    # plot multiple graphs at once in a grid
    def plot_grid(self, plot_matrix):
        np_x = len(plot_matrix)
        np_y = len(plot_matrix[0])

        for ix in range(np_x):
            for iy in range(np_y):
                ax = plt.subplot2grid((np_x, np_y),(ix, iy))
                plot_matrix[ix][iy](self, ax)

        plt.show()

    # PLOT POS HISTROGRAM
    def plot_pos(self, axis=None):
        classes = {}

        # get pos data
        pos_order = []
        for idx, d in enumerate(self.data):
            samp = self.extra.prep(d)
            this_label = self.labels[idx]

            pos_hist = [0]*len(pos_order)
            for pair in samp["pos"]:
                if pair[1] in pos_order:
                    pos_hist[pos_order.index(pair[1])] += 1
                else:
                    pos_order.append(pair[1])
                    pos_hist.append(1)

            if this_label in classes:
                for idx, v in enumerate(classes[this_label]):
                    classes[this_label][idx] += pos_hist[idx]
                if len(pos_hist) > len(classes[this_label]):
                    classes[this_label] += pos_hist[len(classes[this_label]):]
            else:
                classes[this_label] = pos_hist

        # create plot
        if not axis:
            fig, ax = plt.subplots()
        else:
            ax = axis

        bar_x = np.arange(len(pos_order))
        bar_width = 0.3

        rects = []
        for idx, ul in enumerate(classes):
            values = classes[ul]
            rects.append(ax.bar(bar_x + idx * bar_width, values, bar_width, color=self.colors[idx], alpha=0.5))

        ax.set_ylabel("Count")
        ax.set_title("POS Histrogram")
        ax.set_xticks(bar_x + bar_width)
        ax.set_xticklabels(pos_order)

        if not axis:
            plt.show()

    # PLOT AVERAGE WORD AND SENTENCE LENGTH
    def plot_len(self, axis=None):
        classes = {}

        # calc avg sentence and word length
        for idx, d in enumerate(self.data):
            this_label = self.labels[idx]
            tokens = word_tokenize(d)
            sent_len = len(tokens)
            word_len = sum([len(tok) for tok in tokens]) / sent_len

            if this_label not in classes:
                classes[this_label] = [(sent_len, word_len)]
            else:
                classes[this_label].append((sent_len, word_len))

        class_order = []
        sent_avgs = []
        word_avgs = []
        for c in classes:
            sent_avg = 0
            word_avg = 0
            for tpl in classes[c]:
                sent_avg += tpl[0]
                word_avg += tpl[1]

            sent_avg /= len(classes[c])
            word_avg /= len(classes[c])

            class_order.append(c)
            sent_avgs.append(sent_avg)
            word_avgs.append(word_avg)

        # create plot
        if not axis:
            fig, ax = plt.subplots()
        else:
            ax = axis

        bar_x = np.arange(2)
        bar_width = 0.35

        rects = []
        for idx, ul in enumerate(class_order):
            values = (sent_avgs[idx], word_avgs[idx])
            rects.append(ax.bar(bar_x + idx * bar_width, values, bar_width, color=self.colors[idx], alpha=0.5))

        ax.set_ylabel("Length")
        ax.set_title("Avarage Sentence and Word Length")
        ax.set_xticks(bar_x + bar_width)
        ax.set_xticklabels(("sentence", "word"))

        if not axis:
            plt.show()

    # PLOT UNIGRAMS
    def plot_lem(self, axis=None):
        min_count = 5
        new_lem = {}
        for l in self.extra.lem:
            # normalize
            total_count = 0
            for idx, c in enumerate(self.extra.unique_labels):
                if self.extra.lem[l][c] != 0:
                    norm = (self.extra.lem[l][c] / self.extra.label_counts[idx]) * 100
                    total_count += norm

            # leave out uncommon lems, also normal punctuation
            if total_count >= min_count and l not in ".,":
                new_lem[l] = self.extra.lem[l]

        bar_x = np.arange(len(new_lem))
        bar_width = 0.2

        if not axis:
            fig, ax = plt.subplots()
        else:
            ax = axis

        rects = []
        for idx, ul in enumerate(self.extra.unique_labels):
            values = [int(self.extra.lem[l][ul]) for l in new_lem]
            rects.append(ax.bar(bar_x + idx * bar_width, values, bar_width, color=self.colors[idx], alpha=0.5))

        ax.set_ylabel("Appearance (%) per Class")
        ax.set_title("Lemma Histogram")
        ax.set_xticks(bar_x + bar_width)
        ax.set_xticklabels([lemma for lemma in new_lem])

        if not axis:
            plt.show()

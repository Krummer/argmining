#!/usr/bin/python3

import numpy as np

from itertools import combinations
from nltk import pos_tag
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.parse.stanford import StanfordParser
from string import punctuation


class FeatureExtractor:

    # disable word_combi to speed up extraction, but can't use word combinations feature.
    # set up extraction system based on the given samples
    def __init__(self, feat_set, samples):

        self.feat_set = feat_set  # the set used by extraction
        self.feat_suffix = []  # list of found suffixes in functions in set
        self.lem = []  # all lemmas
        self.lem_bi = []  # all lemma bigrams
        self.lem_tri = []  # all lemma trigrams
        self.combi_pair = []  # all word combinations in a statement
        self.punctuation_patterns = []  # all punctuation patterns in a statement

        # set up tools
        self.lemmatizer = WordNetLemmatizer()
        self.feat_suffix = set([fn.__name__.split('_')[-1] for fn in feat_set])

        # prepare extractor.
        # number of n-gram features depends on init samples
        statements = []
        for s in samples:
            statements += s["statements"]

        for idx, st in enumerate(statements):
            st = st.lower()
            tokens = [tok for tok in word_tokenize(st) if tok not in stopwords.words('english')]
            lem_tok = self.lemmatize_tokens(tokens)

            # create final list without punctuation marks
            lemmas = []
            # also create list of punctuation patterns
            pun_pattern = ''
            for l in lem_tok:
                if l in punctuation:
                    if pun_pattern == '':
                        pun_pattern += l
                    elif l == pun_pattern[-1]:
                        pun_pattern += '+'
                    elif len(pun_pattern) > 2 and l == pun_pattern[-2] and pun_pattern[-1] == '+':
                        continue
                    else:
                        pun_pattern += l
                else:
                    lemmas.append(l)
            if pun_pattern not in self.punctuation_patterns:
                self.punctuation_patterns.append(pun_pattern)

            # make ngram feature list
            for l in lemmas:
                if l not in self.lem:
                    self.lem.append(l)
            if "bi" in self.feat_suffix:
                bis = list(ngrams(lemmas, 2))
                for lem_set in bis:
                    if lem_set not in self.lem_bi:
                        self.lem_bi.append(lem_set)
            if "tri" in self.feat_suffix:
                tris = list(ngrams(lemmas, 3))
                for lem_set in tris:
                    if lem_set not in self.lem_tri:
                        self.lem_tri.append(lem_set)
            # make combination features
            if "couples" in self.feat_suffix:
                for c in combinations(lemmas, 2):
                    if c not in self.combi_pair:
                        self.combi_pair.append(c)

    # take a list of tokens and return list of lemmas
    def lemmatize_tokens(self, tokens):
        lem = []
        for tok in tokens:
            tok_lem = self.lemmatizer.lemmatize(tok)
            lem.append(tok_lem)

        return lem

    # get names of all features in the used set (or of another set optionally)
    def get_feature_names(self, f_set=None):
        if not f_set:
            f_set = self.feat_set
        names = []
        for i, fn in enumerate(f_set):
            if "_uni" in fn.__name__:
                names += ["uni_" + l for l in self.lem]
            elif "_bi" in fn.__name__:
                names += ["bi_" + str(b) for b in self.lem_bi]
            elif "_tri" in fn.__name__:
                names += ["tri_" + str(t) for t in self.lem_tri]
            elif "_couples" in fn.__name__:
                names += ["comb_" + str(c) for c in self.combi_pair]
            elif "_pattern" in fn.__name__:
                names += ["pattern_" + str(p) for p in self.punctuation_patterns]
            else:
                names.append(fn.__name__)
        return names

    # feats: feature set (each item is a callback).
    # returns a vector for each statement in the sample. (excluding the claim)
    def extract(self, samples):
        # pre-process the samples.
        p_samples = []
        all_pos = []  # all pos tagged sentences to be able to parse all at once.
        state_n = []  # number of statements per sample in order.
        for s in samples:
            # do basic pre-processing on all texts
            state_n.append(len(s["statements"]))
            s_prep = {"statement": s["statements"], "claim": s["claim"], "token": [], "lemma": [], "pos": []}
            for st in s["statements"]:
                # s_prep["token"].append([tok for tok in word_tokenize(st) if tok not in stopwords.words('english')])
                s_prep["token"].append(word_tokenize(st))
                s_prep["pos"].append(pos_tag(s_prep["token"][-1]))
                s_prep["lemma"].append(self.lemmatize_tokens(s_prep["token"][-1]))

            all_pos += s_prep["pos"]
            p_samples.append(s_prep)

        # get stanford tree parses
        if "tree" in self.feat_suffix:
            parser = StanfordParser()
            all_tree = list(parser.tagged_parse_sents(all_pos))

            for i, s in enumerate(p_samples):
                # fetch associated trees. convert to list because iterators vanish for some reason.
                s["tree"] = [list(t) for t in all_tree[:state_n[i]]]
                all_tree = all_tree[state_n[i]:]

        # finally do the extraction on all samples
        # list with the results from each feature extraction function als elements
        feat_vs = []
        feat_len = []
        for i, samp in enumerate(p_samples):
            for idx in range(0, state_n[i]):
                if idx == samp["claim"]:
                    continue
                state_fs = []
                for ext in self.feat_set:
                    # get results for all samples
                    if "_uni" in ext.__name__:
                        state_fs += ext(samp, idx, self.lem)
                        feat_len.append(len(self.lem))
                    elif "_bi" in ext.__name__:
                        state_fs += ext(samp, idx, self.lem_bi)
                        feat_len.append(len(self.lem_bi))
                    elif "_tri" in ext.__name__:
                        state_fs += ext(samp, idx, self.lem_tri)
                        feat_len.append(len(self.lem_tri))
                    elif "_couples" in ext.__name__:
                        state_fs += ext(samp, idx, self.combi_pair)
                        feat_len.append(len(self.combi_pair))
                    elif "_pattern" in ext.__name__:
                        state_fs += ext(samp, idx, self.punctuation_patterns)
                        feat_len.append(len(self.combi_pair))
                    else:
                        feats = ext(samp, idx)
                        state_fs += feats
                        feat_len.append(len(feats))
                feat_vs.append(state_fs)
        return np.array(feat_vs)

#!/usr/bin/python3

from json import load
from random import shuffle
from nltk.tokenize import sent_tokenize


# returns list of samples and statement labels
def load_samples(json_path):
    with open(json_path, 'r', encoding="UTF-8") as json_file:
        # extract list of labels for each statement. make sure order is preserved
        samples = load(json_file)
        labels = []
        for sa in samples:
            # remove the claim's label
            del sa["labels"][sa["claim"]]
            labels += sa["labels"]

        return samples, labels


# return same amount of data for each class, order not maintained
# TODO: this is kind of obsolete now, because there is no way I can ensure this with the given data ... I think
# using SVM class_weights instead!
def equalize(data, labels, randomize=False):
    class_data = {}
    for i, l in enumerate(labels):
        if l not in class_data:
            class_data[l] = []
        class_data[l].append(data[i])

    min_len = min([len(class_data[l]) for l in class_data])

    new_samples = []
    for l in class_data:
        if randomize:
            shuffle(class_data[l])
        # only take the first x elements
        class_data[l] = class_data[l][:min_len]

        for d in class_data[l]:
            new_samples.append((d, l))

    shuffle(new_samples)
    return zip(*new_samples)


# to be adaptable to multiclass ... but I think this is obsolete
def get_unique_labels(labels):
    return list(set(labels))


# return list with count for each class, also unique label list to maintain order
def get_class_count(labels):
    uni_labels = get_unique_labels(labels)

    counts = []
    for l in uni_labels:
        counts.append(labels.count(l))

    return uni_labels, counts


# return sentences around selected sentence.
# TODO: this is probably also redundant now .... not sure
def get_context(sentence, document):
    s_toks = sent_tokenize(document)
    i_sent = s_toks.index(sentence) if sentence in s_toks else None
    prv = s_toks[i_sent - 1] if i_sent and i_sent != 0 else None
    nxt = s_toks[i_sent + 1] if i_sent and i_sent < len(s_toks) else None

    return prv, nxt
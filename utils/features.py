# a collection of callback-functions for feature extraction

from string import punctuation
from itertools import combinations
from nltk.util import ngrams


# GLOBALS
argument_classifier = None
argument_extractor = None


# HELPER

def get_main_verb(tree):
    verbl = ("VB", "VBG", "VBZ", "VBP", "VBD", "VBN")
    # pick first tree
    for st in tree.subtrees():
        # get outer most VP
        if st.label() == "VP":
            # get first VB
            for sst in st.subtrees():
                if sst.label() in verbl:
                    return sst


# FEATURE FUNCTIONS

# MOCHALES ARGUMENT FEATURES

# modal verb is present
def has_modal(sample, state_idx):
    has = False
    for pair in sample["pos"][state_idx]:
        if "MD" == pair[1]:
            has = True
            break
    return [int(has)]


# statement length (number of tokens)
def sent_length(sample, state_idx):
    return [len(sample["token"][state_idx])]


# avg length of token in statement
def word_length(sample, state_idx):
    total_len = 0
    for s in sample["token"][state_idx]:
        total_len += len(s)

    return [total_len / len(sample["token"][state_idx])]


# count punctuation marks in statement
def punct_count(sample, state_idx):
    cnt = 0
    for s in sample["token"][state_idx]:
        for t in s:
            cnt += int(t in punctuation)
    return [cnt]


# all possible word combinations
def word_couples(sample, state_idx, combi):
    all_comb = []
    for s in sample["lemma"][state_idx]:
        all_comb += combinations(s, 2)
    return [int(c in all_comb) for c in combi]


# punctuation patterns
def punct_pattern(sample, state_idx, patterns):
    ppats = []
    for s in sample["token"][state_idx]:
        ppat = ''
        for t in s:
            if t in punctuation:
                if ppat == '':
                    ppat += t
                elif t == ppat[-1]:
                    ppat += '+'
                elif len(ppat) > 2 and t == ppat[-2] and ppat[-1] == '+':
                    continue
                else:
                    ppat += t
        ppats.append(ppat)

    return [int(p in ppats) for p in patterns]


# depth of parse tree
def depth_tree(sample, state_idx):
    trees = sample["tree"][state_idx]
    tree_n = len(trees)
    if tree_n == 0:
        return [0]
    # use average, sincere there may be more than one possible tree
    total_d = 0
    for t in trees:
        total_d += t.height()
    return [total_d / tree_n]


# number of subclauses in parse tree
def subclauses_tree(sample, state_idx):
    trees = sample["tree"][state_idx]
    tree_n = len(trees)
    if tree_n == 0:
        return [0]
    # average over all possible trees
    total_c = 0
    for t in trees:
        total_c += len(list(t.subtrees()))
    return [total_c / tree_n]


# number of verbs
def num_verb(sample, state_idx):
    num = 0
    for p in sample["pos"][state_idx]:
        if "VB" in p[1]:
            num += 1
    return [num]


# number of adverbs
def num_adv(sample, state_idx):
    num = 0
    for p in sample["pos"][state_idx]:
        if "RB" in p[1]:
            num += 1
    return [num]


def lemma_uni(sample, state_idx, unigrams):
    return [int(u in sample["lemma"][state_idx]) for u in unigrams]


def lemma_bi(sample, state_idx, bigrams):
    state_grams = ngrams(sample["lemma"][state_idx], 2)
    return [int(g in state_grams) for g in bigrams]


def lemma_tri(sample, state_idx, trigrams):
    state_grams = ngrams(sample["lemma"][state_idx], 3)
    return [int(g in state_grams) for g in trigrams]


# MOCHALES PROPOSITION FEATURES

def absolute_location_doc(sample, state_idx):
    # 0.5 means middle. if there is only one sentence, let's say its in the middle.
    # but we usually will get atleast 2 statements anyway (because thats our definition auf an arg.)
    result = 0.5
    if len(sample["statement"]) > 1:
        result = sample["statement"].index(sample["statement"][state_idx])
    return [result]


# TODO rethorical pattern
def reth_pattern(sample, state_idx):
    prv = sample["statement"][state_idx - 1] if state_idx >= 1 else None
    nxt = sample["statement"][state_idx + 1] if state_idx < len(sample["statement"]) else None

    return [0]


# on avarage more than 12 tokens per sentence?
def bin_sent_length(sample, state_idx):
    avg_len = 0

    for s in sample["token"]:
        avg_len += len(s)

    return [int(avg_len/len(sample["token"]) > 12)]


def main_verb_tense_tree(sample, state_idx):
    # 0 no verb, 1 present, 2 past
    pres = ("VB", "VBG", "VBZ", "VBP")
    past = ("VBD", "VBN")
    tree = sample["tree"][state_idx][0]
    mainv = get_main_verb(tree)

    result = 0
    if mainv and mainv.label() in pres:
        result = 1
    elif mainv and mainv.label() in past:
        result = 2

    return [result]


# TODO type of main verb
def main_verb_type(sample, state_idx):
    categories = [
        ["verb"],  # Premise
        ["otherverb"],  # Conclusion
        ["moreverb"],  # Final Decision
    ]

    return [0]


# OTHER FEATURES

# number of sents
def shared_word_count(sample, state_idx):
    cnt = 0
    for tok in sample["token"][state_idx]:
        cnt += int(tok in sample["token"][sample["claim"]])
    return [cnt]


def dist_to_claim(sample, state_idx):
    return [abs(state_idx - sample["claim"])]

# END FEATURE FUNCTIONS

# predefined simple sets
FEATURE_SETS = {
    "binary": (has_modal, lemma_uni, lemma_bi, lemma_tri, word_couples, punct_pattern),
    "all": (shared_word_count, sent_length, word_length, num_verb, num_adv, has_modal, lemma_uni, lemma_bi, lemma_tri,
            depth_tree, subclauses_tree, punct_pattern, punct_count, word_couples, absolute_location_doc, dist_to_claim,
            main_verb_tense_tree),
    "nobags": (shared_word_count, sent_length, word_length, num_verb, num_adv, has_modal, depth_tree, subclauses_tree,
               punct_count, absolute_location_doc, dist_to_claim, main_verb_tense_tree),
    "notree": (shared_word_count, sent_length, word_length, num_verb, num_adv, has_modal, punct_count,
               absolute_location_doc, dist_to_claim),
    "test": (shared_word_count, )
}

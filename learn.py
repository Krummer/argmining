#!/usr/bin/python3

import sys
import utils.data as ud
import numpy as np

from utils.extraction import FeatureExtractor
from utils.features import FEATURE_SETS
from sklearn import svm
from sklearn.preprocessing import normalize
from sklearn.feature_selection import VarianceThreshold, SelectKBest, chi2
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_predict
from sklearn.metrics import accuracy_score, precision_recall_fscore_support, confusion_matrix


if __name__ == "__main__":
    if "-h" in sys.argv or len(sys.argv) == 1:
        print("-----[ USAGE ]-----")
        print("<path to samples> (json) [required]")
        print("-t <path to test samples>")
        print("-p <fraction of data used as test samples> (default 0.2)")
        print("-s <name of feature-set (defualt: nobags)")
        print("-x <number of x-val folds> (int)")
        print("-c <c parameter>")
        print("-var <variance threshold> (float)")
        print("-best <k-best features> (int)")
        print("")
        print("Feature Sets: " + str([fs for fs in FEATURE_SETS]))
        quit()

    j_path = str(sys.argv[1])
    t_path = str(sys.argv[sys.argv.index("-t") + 1]) if "-t" in sys.argv else None
    t_percent = float(sys.argv[sys.argv.index("-p") + 1]) if "-p" in sys.argv else 0.0
    x_folds = int(sys.argv[sys.argv.index("-x") + 1]) if "-x" in sys.argv else 3
    c_param = int(sys.argv[sys.argv.index("-c") + 1]) if "-c" in sys.argv else None
    var_thresh = float(sys.argv[sys.argv.index("-var") + 1]) if "-var" in sys.argv else None
    k_best = int(sys.argv[sys.argv.index("-best") + 1]) if "-best" in sys.argv else None
    feat_set = str(sys.argv[sys.argv.index("-s") + 1]) if "-s" in sys.argv else "nobags"

    # load samples
    samples, labels = ud.load_samples(j_path)
    print("Arguments: {0}".format(len(samples)))
    print("-----[ EXTRACT ]-----")
    extractor = FeatureExtractor(FEATURE_SETS[feat_set], samples)
    feature_vs = normalize(extractor.extract(samples))
    feat_names = extractor.get_feature_names()

    # separate test data
    t_samples, t_labels = None, None
    if t_percent > 0:
        feature_vs, t_samples, labels, t_labels = train_test_split(feature_vs, labels, test_size=t_percent, random_state=0)
    elif t_path:
        t_samples, t_labels = ud.load_samples(t_path)
        t_samples = normalize(extractor.extract(t_samples))

    # report data
    class_dist = [0, 0]
    for l in labels:
        class_dist[l] += 1
    print("Statement Samples: {0}".format(len(feature_vs)))
    print(">> Support: {0}, Attack: {1}".format(class_dist[0], class_dist[1]))
    print("Features: {0}".format(len(feature_vs[0])))

    # perform feature selection
    if var_thresh:
        selector = VarianceThreshold(threshold=var_thresh)
        feature_vs = selector.fit_transform(feature_vs)
        if t_path or t_percent > 0:
            t_samples = selector.transform(t_samples)
        print("Minus 0-Variance: {0}".format(len(feature_vs[0])))

        # print names of selected features
        mask = np.array(selector.get_support())
        feat_names = np.ma.array(feat_names, mask=np.logical_not(mask)).compressed()
        print("[%s]" % ', '.join(map(str, feat_names)))

    if k_best:
        selector = SelectKBest(chi2, k_best)
        feature_vs = selector.fit_transform(feature_vs, labels)
        if t_path or t_percent > 0:
            t_samples = selector.transform(t_samples)
        print("{0}-Best Features:".format(k_best))
        # print names of selected features
        mask = np.array(selector.get_support())
        feat_names = np.ma.array(feat_names, mask=np.logical_not(mask)).compressed()
        print("[%s]" % ', '.join(map(str, feat_names)))

    # train
    print("-----[ TRAIN ]-----")
    c_range = [pow(2, d) for d in range(-5, 15)]
    parameters = [{"kernel": ["linear"], 'C': c_range},
                  {"kernel": ["rbf"], 'C': c_range, 'gamma': [pow(2, d) for d in range(-15, 3)]}]
    svr = svm.SVC(C=0, gamma='auto', kernel="linear", class_weight="balanced")
    clf = GridSearchCV(svr, parameters, n_jobs=2, cv=x_folds)
    clf.fit(feature_vs, labels)
    print("Kernel: " + str(clf.best_params_["kernel"]))
    print("C: " + str(clf.best_params_['C']))
    if clf.best_params_["kernel"] == "rbf":
        print("gamma: " + str(clf.best_params_["gamma"]))
    print("score: " + str(clf.score(feature_vs, labels)))

    # test
    if t_labels:
        t_dist = [0, 0]
        for l in t_labels:
            t_dist[l] += 1
        print("-----[ TEST  ]-----")
        print("Statement Samples: {0}".format(len(t_labels)))
        print(">> Support: {0}, Attack: {1}".format(t_dist[0], t_dist[1]))
        pred = clf.predict(t_samples)
        labels = t_labels
    else:
        print("-----[ CROSSVALIDATION ]-----")
        pred = cross_val_predict(clf, feature_vs, labels, cv=x_folds)
        # print(">> accuracy: {0} (+/- {1})".format(scores.mean().round(2), (scores.std() * 2).round(2)))

    # report
    print(">> accuracy: " + str(accuracy_score(labels, pred)))
    precision, recall, fscore, _ = precision_recall_fscore_support(labels, pred)
    print(">> precision: " + str(precision))
    print(">> recall: " + str(recall))
    print(">> f-score: " + str(fscore))
    print(">> confusion matrix:")
    print(confusion_matrix(labels, pred))
